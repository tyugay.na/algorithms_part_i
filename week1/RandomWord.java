/* *****************************************************************************
 *  Name:              Ada Lovelace
 *  Coursera User ID:  123456
 *  Last modified:     October 16, 1842
 **************************************************************************** */


import edu.princeton.cs.algs4.StdIn;
import edu.princeton.cs.algs4.StdOut;
import edu.princeton.cs.algs4.StdRandom;

public class RandomWord {
    public static void main(String[] args) {
        String str = "", champion = "";
        int i = 0;
        while (true) {
            str = StdIn.readString();
            i++;
            if (StdRandom.bernoulli(1.0 / i)) {
                champion = str;
            }
            if (StdIn.isEmpty()) {
                break;
            }
        }
        StdOut.println(champion);
    }
}
